
// Test d'exemple par défaut :
describe('EdenAI web site login page', () => {
    it('successfully loads', () => {
        cy.visit('https://app.edenai.run')
    })
})

describe('EdenAI web site forgot-password page', () => {
	//tester que la page  forgot-password se charge bien
    it('successfully load forgot-password page', () => {
		cy.contains('Forgot password?').click()
        cy.url().should('eq', 'https://app.edenai.run/user/forgot-password')         
    })

    it('successfully alert ', () => {
    //Afficher une alert quand le format de mail n'est pas valide
        cy.get('#__BVID__51').clear().type('test')
        cy.get('.d-flex > .btn').click()
        cy.get('.alert')
     
    })
    
	//tester que le boutton Back to login 
    it('successfully load login page', () => {
		cy.contains('Back to login').click()
        cy.url().should('eq', 'https://app.edenai.run/user/login')         
    })

})

describe('EdenAI web site register page', () => {
	//tester que la page  register se charge bien
    it('successfully load register page', () => {
		cy.contains('Register').click()
        cy.url().should('eq', 'https://app.edenai.run/user/register')         
    })

    it('successfully alert ', () => {
    //Afficher une alert quand l'utlisateur n'a pas cocher I agree to the Terms  
        cy.get('.d-flex > .btn').click()
        cy.get('.alert')
            
    })  
    //tester le boutton Login 
     it('successfully load home page', () => {
        cy.contains('Login').click()
        cy.url().should('eq', 'https://app.edenai.run/user/login')         
    })
 
})


describe('connexion in EdenAI ', () => {
	// se connecter à l'app
    it('successfully connect in the app', () => {
        cy.get('#inputloginemail').clear().type('faroukbenrabh@gmail.com')
        cy.get('#inputloginpassword').clear().type('Majid3223')
        cy.get('.d-flex > .btn').click()
        cy.url().should('eq', 'https://app.edenai.run/bricks/default')         

    })
})


describe('EdenAI web site Home page ', () => {
	// Navigation bar
    it('successfully navigation in the app', () => {
        cy.get('.menu-button > .text-center').click()
        cy.get('a > .simple-icon-eye').click()
        cy.url().should('eq', 'https://app.edenai.run/bricks/vision/default') 

        cy.get('.menu-button > .text-center').click()
        cy.get('.iconsminds-open-book').click()
        cy.url().should('eq', 'https://app.edenai.run/bricks/text/default')      
        
        cy.get('.menu-button > .text-center').click()
        cy.get('.iconsminds-letter-open').click()
        cy.url().should('eq', 'https://app.edenai.run/bricks/ocr/default')    
        
        cy.get('.menu-button > .text-center').click()
        cy.get('[data-flag="translation"]').click()
        cy.url().should('eq','https://app.edenai.run/bricks/translation/default')  
        
        cy.get('.menu-button > .text-center').click()
        cy.get('.iconsminds-coins').click()
        cy.url().should('eq','https://app.edenai.run/admin/cost-management')   
       

    })
})

describe('EdenAI web site Sing out ', () => {

    it('Sing out', () => {
        cy.get('.name').click()
        cy.get(':nth-child(3) > .dropdown-item').click()
        cy.url().should('eq', 'https://app.edenai.run/user/login')    

   })
})



describe('EdenAI web site Translate page ', () => {
    before(function () {
      cy.visit('https://app.edenai.run')
   
    })
     it('exemple to translate', () => {

        cy.get('#inputloginemail').type('faroukbenrabh@gmail.com')
        cy.get('#inputloginpassword').type('Majid3223')
        cy.get('.d-flex > .btn').click()
    cy.get('.menu-button > .text-center').click()
     cy.get('[data-flag="translation"]').click()
     cy.wait(2000)
      cy.get('.card-body').eq(0).click()
        
       cy.get('#select_source_language').select('fr-FR')
       cy.get('#select_target_language').select('en-US')
       cy.get('#textarea').clear().type('Bonjour')
       cy.get('#launch_benchmark_button').click()

       cy.get('.card-body').contains('Hello')
       
    })
})

describe('EdenAI web site OCR page ', () => {
    before(function () {
        cy.visit('https://app.edenai.run')
     
      })
    it('exemple to OCR', () => {
        cy.get('#inputloginemail').type('faroukbenrabh@gmail.com')
        cy.get('#inputloginpassword').type('Majid3223')
        cy.get('.d-flex > .btn').click()

        cy.get('.menu-button > .text-center').click()
        cy.get('[data-flag="ocr"]').click()
    
        cy.wait(2000)
         cy.get('.card-body').eq(0).click()

         const filepath = 'images/NoEntryWhenRed.jpg'
         
         cy.get('input[id="file_input"]').attachFile(filepath)
         cy.get('.input-group-text').click()
         cy.get('#select_language').select('en-US')
         cy.get('#launch_benchmark_button').click()
         cy.get('.pl-3 > .text-muted').contains('NO ENTRY WHEN RED LIGHT IS FLASHING')



   })
})

